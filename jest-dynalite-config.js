module.exports = {
  tables: [
    {
      TableName: `Tile`,
      KeySchema: [
        { AttributeName: "ClusterId", KeyType: "HASH" },
        { AttributeName: "TileId", KeyType: "RANGE" },
      ],
      AttributeDefinitions: [{ AttributeName: "ClusterId", AttributeType: "S" }, { AttributeName: "TileId", AttributeType: "S" }],
      ProvisionedThroughput: { ReadCapacityUnits: 1, WriteCapacityUnits: 1 },
    },
    // etc
  ]
};
