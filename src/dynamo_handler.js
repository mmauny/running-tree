const AWS = require("aws-sdk");
const { buildWorld, move } = require("./service/world");

const docClient = new AWS.DynamoDB.DocumentClient();

async function getPosition() {
  try {
    const params = {
      TableName: "Position",
      Key: {
        UserId: "101566597018899627343",
      },
    };
    const data = await docClient.get(params).promise();
    return data.Item;
  } catch (err) {
    return err;
  }
}

exports.getPosition = async (event, context) => {
  try {
    const data = await getPosition();
    return {
      headers: {
        "Access-Control-Allow-Origin": "https://running-tree.larus.fr",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify(data),
    };
  } catch (err) {
    return { error: err };
  }
};

exports.move = async (event, context) => {
  try {
    const position = await getPosition();
    const payload = JSON.parse(event.body)
    const data = await move(payload.direction, position);
    return {
      headers: {
        "Access-Control-Allow-Origin": "https://running-tree.larus.fr",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify(data),
    };
  } catch (err) {
    return { error: err };
  }
};

exports.getWorld = async (event, context) => {
  try {
    const position = await getPosition();
    const data = await buildWorld(position);
    return {
      headers: {
        "Access-Control-Allow-Origin": "https://running-tree.larus.fr",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify(data),
    };
  } catch (err) {
    return { error: err };
  }
};
