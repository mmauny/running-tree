const AWS = require("aws-sdk");

const isTest = process.env.JEST_WORKER_ID;

const config = {
  convertEmptyValues: true,
  ...(process.env.MOCK_DYNAMODB_ENDPOINT && {
    endpoint: process.env.MOCK_DYNAMODB_ENDPOINT,
    sslEnabled: false,
    region: "local",
  }),
};

const docClient = new AWS.DynamoDB.DocumentClient(config);

async function queryWorld(clusterId) {
  try {
    var params = {
      TableName: "Tile",
      KeyConditionExpression: "#clstr = :id",
      ExpressionAttributeNames: {
        "#clstr": "ClusterId",
      },
      ExpressionAttributeValues: {
        ":id": clusterId,
      },
    };
    const data = await docClient.query(params).promise();
    return data.Items;
  } catch (err) {
    return err;
  }
}


async function getTile(ClusterId, TileId){
  try {
    const params = {
      TableName : 'Tile',
      Key: {
        ClusterId,
        TileId
      }
    }
    const data = await docClient.get(params).promise()
    return data.Item
  } catch (err) {
    return err
  }
}

async function putTile(tile) {
  try {
    var params = {
      TableName: "Tile",
      Item: tile,
    };
    await docClient.put(params).promise();
  } catch (err) {
    return err;
  }
}

async function putPosition(position) {
  try {
    var params = {
      TableName: "Position",
      Item: position,
    };
    await docClient.put(params).promise();
  } catch (err) {
    return err;
  }
}

module.exports = { queryWorld, putTile, putPosition, getTile };
