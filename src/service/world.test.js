const {getClusterKey, buildWorld, indexForCache} = require("./world");
const db= require("./db");

test("center in cluster 0_0", () => {
  expect(getClusterKey({ x: 0, y: 0 })).toBe("0_0");
});

test("{x:9, y:0} in cluster 0_0", () => {
  expect(getClusterKey({ x: 9, y: 0 })).toBe("0_0");
});

test("{x:9, y:9} in cluster 0_0", () => {
  expect(getClusterKey({ x: 9, y: 9 })).toBe("0_0");
});

test("{x:10, y:0} in cluster 1_0", () => {
  expect(getClusterKey({ x: 10, y: 0 })).toBe("1_0");
});

test("{x:11, y:0} in cluster 1_0", () => {
  expect(getClusterKey({ x: 11, y: 0 })).toBe("1_0");
});

test("{x:21, y:0} in cluster 2_0", () => {
  expect(getClusterKey({ x: 21, y: 0 })).toBe("2_0");
});

test("{x:0, y:10 } in cluster 0_1", () => {
  expect(getClusterKey({ x: 0, y: 10 })).toBe("0_1");
});

test("{x:10, y:11} in cluster 1_1", () => {
  expect(getClusterKey({ x: 10, y: 11 })).toBe("1_1");
});

test("{x:11, y:21} in cluster 1_2", () => {
  expect(getClusterKey({ x: 11, y: 21 })).toBe("1_2");
});

test("{x:21, y:23} in cluster 2_2", () => {
  expect(getClusterKey({ x: 21, y: 23 })).toBe("2_2");
});

test("build one tile world", async () => {
  const tile =     {
    "ClusterId":"0_0",
    "type": "CITY",
    "coord": { "x": 0, "y": 0 },
    "name": "Nantes",
    "TileId": "0_0"
  };
    await db.putTile(tile)
    const world = await buildWorld({ x: 0, y: 0 });
    expect(world.filter(a => a.type == "DESERT").length).toBe(80);
});

test("simple indexation", () => {
    const tile =     {
        "ClusterId":"0_0",
        "type": "CITY",
        "coord": { "x": 0, "y": 0 },
        "name": "Nantes",
        "TileId": "0_0"
      };

    const result = indexForCache([tile])
    console.log('result cache',result)
    expect(result['0_0']).toBe(tile);
});