const { putTile, queryWorld, getTile } = require("./db");

it('should insert item into table', async () => {
    const tile =     {
        "ClusterId":"0_0",
        "type": "CITY",
        "coord": { "x": 0, "y": 0 },
        "name": "Nantes",
        "TileId": "0_0"
      };
    await putTile(tile)
    const result = await queryWorld('0_0')
    expect(result.length).toBe(1)
  });

  it('should insert item into table and get it', async () => {
    const tile =     {
        "ClusterId":"0_0",
        "type": "CITY",
        "coord": { "x": 0, "y": 0 },
        "name": "Me",
        "TileId": "0_1"
      };
    await putTile(tile)
    const result = await getTile('0_0', "0_1")
    expect(result.name).toBe("Me")
  });

  it('should not get it', async () => {
    const tile =     {
        "ClusterId":"0_0",
        "type": "CITY",
        "coord": { "x": 0, "y": 0 },
        "name": "Me",
        "TileId": "0_1"
      };
    await putTile(tile)
    const result = await getTile('0_1', "0_1")
    expect(result).toBe(undefined)
  });


  it('should not insert item into table Missing ID', async () => {
    const tile =     {
        "type": "CITY",
        "coord": { "x": 0, "y": 0 },
        "name": "Nantes",
        "TileId": "0_0"
      };
    const error = await putTile(tile) 
    expect(error.code).toBe("ValidationException")
  });
  

  it('should not query items (missing key)', async () => {
    const tile =     {
        "ClusterId":"0_0",
        "type": "CITY",
        "coord": { "x": 0, "y": 0 },
        "name": "Nantes",
        "TileId": "0_0"
      };
    await putTile(tile)
    const error = await queryWorld()
    expect(error.code).toBe("ValidationException")
  });