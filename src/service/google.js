const { google } = require("googleapis");

const client_id = process.env.CLIENT_ID;
const client_secret = process.env.CLIENT_SECRET;


const oauth2Client = new google.auth.OAuth2(
  client_id,
  client_secret,
  "https://api.running-tree.larus.fr/oauth2callback"
);

const scopes = [
  "https://www.googleapis.com/auth/fitness.location.read",
  "https://www.googleapis.com/auth/fitness.activity.read",
  "https://www.googleapis.com/auth/userinfo.profile",
];

const fitness = google.fitness("v1");
const oauth2 = google.oauth2({
  version: "v2",
});

google.options({ auth: oauth2Client });

const authorizeUrl = oauth2Client.generateAuthUrl({
    access_type: "offline",
    scope: scopes.join(" "),
  });

async function me(access_token) {
    oauth2Client.credentials.access_token = access_token;

    const res = await oauth2.userinfo.get();
  
    return res.data.id;
}

module.exports = { me };
