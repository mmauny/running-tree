const CLUSTER_SIZE = 10;
const VISION = 4;

const getKey = ({ x, y }) => `${x}_${y}`;

const db = require("./db");

function getClusterKey({ x, y }) {
  return getKey({
    x: Math.floor(x / CLUSTER_SIZE),
    y: Math.floor(y / CLUSTER_SIZE),
  });
}

const indexForCache = (tiles) =>
  tiles.reduce((acc, val) => ({ ...acc, [val.TileId]: val }), {});

async function buildWorld(position) {
  const clusterCache = {};
  const world = [];
  const { x, y } = position;
  for (let i = x + VISION; i >= x - VISION; i--) {
    for (let j = y + VISION; j >= y - VISION; j--) {
      const currentTile = { x: i, y: j };
      const currentCluster = getClusterKey(currentTile);
      if (!clusterCache[currentCluster]) {
        const tiles = await db.queryWorld(currentCluster);
        clusterCache[currentCluster] = indexForCache(tiles);
      }
      const currentTileKey = getKey(currentTile);
      if (clusterCache[currentCluster][currentTileKey]) {
        world.push(clusterCache[currentCluster][currentTileKey]);
      } else {
        world.push({
          type: "DESERT",
          coord: currentTile,
          TileId: currentTileKey,
        });
      }
    }
  }
  return world;
}

const add = (a, b) => ({ x: a.x + b.x, y: a.y + b.y });

const newTile = (position) => ({
  ClusterId: getClusterKey(position),
  type: "GRASS",
  coord: position,
  TileId: getKey(position),
});

async function move(direction, position) {
  const directionToMove = {
    N: { x: 0, y: 1 },
    S: { x: 0, y: -1 },
    E: { x: 1, y: 0 },
    W: { x: -1, y: 0 },
  };
  const newPosition = {
    ...position,
    ...add(directionToMove[direction], position),
  };
  await db.putPosition(newPosition);
  const currentTile = await db.getTile(getClusterKey(newPosition), getKey(newPosition));
  if (!currentTile) {
    await db.putTile(newTile(newPosition));
  }
  return newPosition;
}

module.exports = { getClusterKey, buildWorld, indexForCache, move };
