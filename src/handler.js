"use strict";

const { google } = require("googleapis");
const cookie = require("cookie");

const {me} = require('./service/google')

const client_id = process.env.CLIENT_ID;
const client_secret = process.env.CLIENT_SECRET;
const access_cookie_name = "ACCESS"

const oauth2Client = new google.auth.OAuth2(
  client_id,
  client_secret,
  "https://api.running-tree.larus.fr/oauth2callback"
);

const scopes = [
  "https://www.googleapis.com/auth/fitness.location.read",
  "https://www.googleapis.com/auth/fitness.activity.read",
  "https://www.googleapis.com/auth/userinfo.profile"
];

const fitness = google.fitness("v1");

google.options({ auth: oauth2Client });

const authorizeUrl = oauth2Client.generateAuthUrl({
  access_type: "offline",
  scope: scopes.join(" "),
});

module.exports.login = async (event) => {
  const response = {
    statusCode: 301,
    headers: {
      Location: authorizeUrl,
    },
  };
  return response;
};

module.exports.callback = async (event) => {
  const code = event.queryStringParameters.code;
  const { tokens } = await oauth2Client.getToken(code);
  oauth2Client.credentials = tokens;
  return {
    statusCode: 301,
    headers: {
      Location: "https://running-tree.larus.fr",
      "Set-Cookie": cookie.serialize(access_cookie_name, tokens.access_token, {
        path: '/',
        sameSite: 'none',
        secure: true,
        httpOnly: true,
        expires: new Date(new Date().getTime() + 10 * 1000),
      }),
    },
  };
};

module.exports.me = async (event) => {
  const {headers : { cookie: cookieString}} = event;
  console.log('event',event)
  const access_token = cookie.parse(cookieString)[access_cookie_name];
  console.log('access_token',access_token)
  const id = await me(access_token);
  return {
    statusCode: 200,
    headers : {
      "Access-Control-Allow-Origin": "https://running-tree.larus.fr",
      "Access-Control-Allow-Credentials": true,
    },
    body: JSON.stringify(
      {
        id,
      },
      null,
      2
    ),
  };
};

module.exports.getDistance = async (event) => {
  const {headers : { cookie: cookieString}} = event;
  const access_token = cookie.parse(cookieString)[access_cookie_name];
  oauth2Client.credentials.access_token = access_token;

  const res = await fitness.users.dataset.aggregate({
    userId: "me",
    requestBody: {
      aggregateBy: [
        {
          dataTypeName: "com.google.step_count.delta",
          dataSourceId:
            "derived:com.google.distance.delta:com.google.android.gms:merge_distance_delta",
        },
      ],
      bucketByTime: { durationMillis: 86400000 },
      startTimeMillis: 1641078000000,
      endTimeMillis: 1641164340000,
    },
  });
  const {data} = res;
  const meters = data.bucket[0].dataset[0].point[0].value[0].fpVal
  return {
    statusCode: 200,
    headers : {
      "Access-Control-Allow-Origin": "https://running-tree.larus.fr",
      "Access-Control-Allow-Credentials": true,
    },
    body: JSON.stringify(
      {
        message: meters,
      },
      null,
      2
    ),
  };
};

